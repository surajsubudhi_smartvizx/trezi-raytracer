#include "inc/Camera/Camera.h"

#include "inc/math_utils/Vector2.h"
#include "Arcball.h"
#include "inc/StaticFunc.h"

namespace TRAY
{
	Camera::Camera() :
		mWidth(960), mHeight(540), mFov(35.0f),
		mCamType(PINHOLE),
		mCameraEye(0.0f, 0.0f, 0.0f),
		mCameraLookAt(0.0f, 0.0f, -1.0f),
		mCameraUP(0.0f, 1.0f, 0.0f),
		mCameraTransform(Matrix4F(1.0f)),
		mInitialLookAtPos(0.0f, 0.0f, -1.0f)
	{
		isCameraChanged = true;

		mPrevCameraEye = mCameraEye;
		mPrevCameraU = mCameraU;
		mPrevCameraV = mCameraV;
		mPrevCameraW = mCameraW;

		ComputeUVW();
	}

	Camera::Camera(unsigned int aWidth, unsigned int aHeight, const Vector3F& aCameraEye, const Vector3F& aCameraLookat, const Vector3F& aCameraUP, eCameraType aCamType) :
		mWidth(aWidth), mHeight(aHeight), mFov(35.0f),
		mCamType(aCamType),
		mCameraEye(aCameraEye),
		mCameraLookAt(aCameraLookat),
		mCameraUP(aCameraUP),
		mCameraTransform(Matrix4F(1.0f)),
		mInitialLookAtPos(aCameraLookat)
	{
		isCameraChanged = true;

		mPrevCameraEye = mCameraEye;
		mPrevCameraU = mCameraU;
		mPrevCameraV = mCameraV;
		mPrevCameraW = mCameraW;

		ComputeUVW();
	}

	void Camera::ResetLookAt()
	{
		const Vector3F translation = mInitialLookAtPos - mCameraLookAt;
		mCameraEye += translation;
		mCameraLookAt = mInitialLookAtPos;
		isCameraChanged = true;
		ComputeUVW();
	}

	bool Camera::Resize(unsigned int aWidth, unsigned int aHeight)
	{
		if (aWidth == mWidth && aHeight == mHeight)
			return false;

		mWidth = aWidth;
		mHeight = aHeight;
		isCameraChanged = true;
		ComputeUVW();

		return true;
	}

	void Camera::MoveForward(float speed)
	{
		mCameraEye = mCameraEye + (mCameraW)* speed;
		mCameraLookAt = mCameraLookAt + (mCameraW)* speed;
		isCameraChanged = true;
		ComputeUVW();
	}

	void Camera::MoveRight(float speed)
	{
		mCameraEye = mCameraEye + (mCameraU)* speed;
		mCameraLookAt = mCameraLookAt + (mCameraU)* speed;
		isCameraChanged = true;
		ComputeUVW();
	}

	void Camera::MoveUp(float speed)
	{
		mCameraEye = mCameraEye + (mCameraV)* speed;
		mCameraLookAt = mCameraLookAt + (mCameraV)* speed;
		isCameraChanged = true;
		ComputeUVW();
	}

	void Camera::PrintCamData() const
	{
		std::cout << "Cam Pos : (" << mCameraEye.x << ", " << mCameraEye.y << ", " << mCameraEye.z << ")\n";
		std::cout << "Cam LookAt : (" << mCameraLookAt.x << ", " << mCameraLookAt.y << ", " << mCameraLookAt.z << ")\n";
		std::cout << "Cam UP : (" << mCameraUP.x << ", " << mCameraUP.y << ", " << mCameraUP.z << ")\n";
	}

	void Camera::SetSanpshotPoint()
	{
		mCameraEye = Vector3F(-0.312f, 28.821f, 65.5723f);
		mCameraLookAt = Vector3F(0.0f, 6.694f, 0.0f);
		mCameraUP = Vector3F(0.0f, 0.98f, -0.17f);
		isCameraChanged = true;
		ComputeUVW();
	}

	bool Camera::ProcessMouse(float x, float y, bool left_button_down, bool right_button_down, bool middle_button_down)
	{
		static sutil::Arcball arcball;
		static Vector2F mouse_prev_pos = Vector2F(0.0f, 0.0f);
		static bool   have_mouse_prev_pos = false;

		// No action if mouse did not move
		if (mouse_prev_pos.x == x && mouse_prev_pos.y == y)
			return false;

		bool dirty = false;

		if (left_button_down || right_button_down || middle_button_down)
		{
			if (have_mouse_prev_pos)
			{
				if (left_button_down)
				{

					const Vector2F from = { mouse_prev_pos.x, mouse_prev_pos.y };
					const Vector2F to = { x, y };

					const Vector2F a = { from.x / mWidth, from.y / mHeight };
					const Vector2F b = { to.x / mWidth, to.y / mHeight };

					mCameraTransform = CastToMatrix4F(arcball.rotate(optix::make_float2(b.x, b.y), optix::make_float2(a.x, a.y)));

				}
				else if (right_button_down)
				{
					const float dx = (x - mouse_prev_pos.x) / mWidth;
					const float dy = (y - mouse_prev_pos.y) / mHeight;
					const float dmax = fabsf(dx) > fabs(dy) ? dx : dy;
					const float scale = fminf(dmax, 0.9f);

					mCameraEye = mCameraEye + (mCameraLookAt - mCameraEye) * scale;

				}
				else if (middle_button_down)
				{
					const float dx = (x - mouse_prev_pos.x) / mWidth;
					const float dy = (y - mouse_prev_pos.y) / mHeight;

					Vector3F translation = -dx * mCameraU + dy * mCameraV;
					mCameraEye = mCameraEye + translation;
					mCameraLookAt = mCameraLookAt + translation;
				}

				isCameraChanged = true;
				ComputeUVW();
				dirty = true;
			}

			have_mouse_prev_pos = true;
			mouse_prev_pos.x = x;
			mouse_prev_pos.y = y;

		}
		else
		{
			have_mouse_prev_pos = false;
		}
		return dirty;
	}

	Matrix4F Camera::GetCameraTransformMatrix() const
	{
		Vector3F W = (mCameraLookAt - mCameraEye).norm(); // Do not normalize W -- it implies focal length
		Vector3F U = (Cross(mCameraUP, W)).norm();
		Vector3F V = (Cross(W, U));

		Matrix4F result;
		result.set_column(0, Vector4F(U, -Dot(U, mCameraEye)));
		result.set_column(1, Vector4F(W, -Dot(W, mCameraEye)));
		result.set_column(2, Vector4F(V, -Dot(V, mCameraEye)));
		result.set_column(3, Vector4F(0.0f, 0.0f, 0.0f, 1.0f));
		return result.transpose();
	}

	bool Camera::GetCameraCoordinateFrame(Vector3F & oU, Vector3F & oV, Vector3F & oW, Vector3F & oEyePos)
	{
		if (isCameraChanged) 
		{
			ComputeUVW();

			oU = mCameraU;
			oV = mCameraV;
			oW = mCameraW;
			oEyePos = mCameraEye;
			isCameraChanged = false;
			return true;
		}

		return false;
	}

	void Camera::SetCameraData(const Matrix4F & aCamTransMat, float aFov, float aWidth, float aHeight, eCameraType aCamType)
	{
		Resize((unsigned int)aWidth, (unsigned int)aHeight);

		mFov = aFov;
		mCamType = aCamType;

		// Mapping the coordinate system of Unreal i.e Left Hand 
		mCameraEye = makeVec3(aCamTransMat*Vector4F(0.0f, 0.0f, 0.0f, 1.0f));
		mCameraLookAt = makeVec3(aCamTransMat*Vector4F(1.0f, 0.0f, 0.0f, 1.0f));
		mCameraUP = makeVec3(aCamTransMat*Vector4F(0.0f, 0.0f, 1.0f, 0.0f));
		isCameraChanged = true;
		ComputeUVW();
	}

	void Camera::SetCameraData(const Vector3F & aCamEye, const Vector3F & aCamLookAt, const Vector3F& aUp)
	{
		mCameraEye = aCamEye;
		mCameraLookAt = aCamLookAt;
		mCameraUP = aUp;
		isCameraChanged = true;
		ComputeUVW();
	}

	void Camera::ComputeUVW()
	{
		const bool cameraChanged = isCameraChanged;
		if (cameraChanged)
		{
			const float vfov = mFov;
			const float aspectRatio = static_cast<float>(mWidth) / static_cast<float>(mHeight);

			CalculateCameraVariables(mCameraEye, mCameraLookAt, mCameraUP, vfov, aspectRatio, false, mCameraU, mCameraV, mCameraW);
			const Matrix4F frame = FromBasis(mCameraU, mCameraV, -mCameraW, mCameraLookAt);
			const Matrix4F frameInverse = frame.inverse();

			// Apply camera rotation twice to match old SDK behavior
			const Matrix4F trans = frame * mCameraTransform * mCameraTransform * frameInverse;

			mCameraEye = makeVec3(trans * Vector4F(mCameraEye, 1.0f));
			mCameraLookAt = makeVec3(trans * Vector4F(mCameraLookAt, 1.0f));
			mCameraUP = makeVec3(trans * Vector4F(mCameraUP, 0.0f));

			CalculateCameraVariables(mCameraEye, mCameraLookAt, mCameraUP, vfov, aspectRatio, false, mCameraU, mCameraV, mCameraW);
			mCameraTransform = Matrix4F(1.0f);

			mPrevCameraEye = mCameraEye;
			mPrevCameraU = mCameraU;
			mPrevCameraV = mCameraV;
			mPrevCameraW = mCameraW;

			
		}
	}

	void Camera::CalculateCameraVariables(const Vector3F & aEye, const Vector3F & aLookAt, const Vector3F & aUp,
		float aFov, float aAspectRatio, bool aFovIsVertical,
		Vector3F & U, Vector3F & V, Vector3F & W) const
	{
		float ulen, vlen;
		W = aLookAt - aEye; // Do not normalize W -- it implies focal length

		const float wlen = (W).length();
		U = (Cross(aUp, W)).norm();
		V = (Cross(W, U)).norm();

		if (aFovIsVertical)
		{
			vlen = wlen * tanf(0.5f * mFov * M_PIf / 180.0f);
			V *= vlen;
			ulen = vlen * aAspectRatio;
			U *= ulen;
		}
		else
		{
			ulen = wlen * tanf(0.5f * mFov * M_PIf / 180.0f);
			U *= ulen;
			vlen = ulen / aAspectRatio;
			V *= vlen;
		}
	}
}
