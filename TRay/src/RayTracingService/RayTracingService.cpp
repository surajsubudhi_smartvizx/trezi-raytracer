#include "inc/RayTracingService/RayTracingService.h"
#include <inc/math_utils/Matrix4.h>
#include <inc/TRayNode.h>
#include <inc/Application.h>
#include "inc/Json/json.hpp"
#include "inc/Camera/Camera.h"
#include <algorithm>
#include <inc/OptixTracer.h>

namespace TRAY
{
	namespace
	{
		int TotalFramesToRender = 1200;
		constexpr int MaxFrameLimit = 2200;
		constexpr int MinFrameLimit = 500;
		constexpr size_t MaxNumOfTriangles = 800000;
	}

	RayTracingService::RayTracingService(Application* App, unsigned int Port)
		: TNListener()
		, mTNApplication(nullptr)
		, mTRayApp(App)
	{
		mTNApplication = TreziNet::TNApplication::CreateServerApplication(Port, false, MaxAllowedConnections);

		TreziNet::TNUserInfo UserInfo;
		UserInfo.UserUniqueID = "SomeUniqueRandomString";
		UserInfo.UserName = "TRay";

		mTNApplication->Init(UserInfo);

		mTNApplication->Connect();
	}

	RayTracingService::~RayTracingService()
	{
		if (mTNApplication)
		{
			TreziNet::TNApplication::DestroyApplication(mTNApplication);
		}
	}

	void RayTracingService::Update()
	{
		if (mTNApplication)
		{
			mTNApplication->Update();

			if (mCurrentProcessing.first == nullptr || mCurrentProcessing.second == nullptr)
			{
				mCurrentProcessing = GetAndRemoveRequestFromList(mRequestsPending);
				if (mCurrentProcessing.first != nullptr && mCurrentProcessing.second != nullptr)
				{
					mCurrentProcessing.second->LoadScene(mTRayApp);
					mRenderingFrameCount = 0;
				}
			}
			else if (mCurrentProcessing.second->IsProcessing()) // and also check if screenshot is done;
			{
				mRenderingFrameCount++;
				float t = (float)mTRayApp->GetNumOfTriangle() / (float)MaxNumOfTriangles;
				TotalFramesToRender = (MaxFrameLimit * t) + MinFrameLimit * (1.0f - t);
				if (mRenderingFrameCount >= TotalFramesToRender)
				{
					if (mCurrentProcessing.second->CreateImageAndSendResponse(mTRayApp))
					{
						mCurrentProcessing = std::pair<TNSocket*, SnapshotRequest*>(nullptr, nullptr);
					}
					mRenderingFrameCount = 0;
					//mTRayApp->ExitApp();
				}
				else
				{
					const float Percent = 100.0f * (float)mRenderingFrameCount / (float)TotalFramesToRender;
					mCurrentProcessing.second->SendRequestFeedback(Percent);
				}
			}
		}
	}

	void RayTracingService::OnClientConnected(TNSocket* Client)
	{
		printf("Client connected...\n");
		mTRayApp->SetRenderingState(false);
	}

	void RayTracingService::OnClientDisConnected(TNSocket* Client)
	{
		RemoveUserFromList(mRequestsComingIn, Client);
		RemoveUserFromList(mRequestsPending, Client);
		if (mCurrentProcessing.first == Client)
		{
			delete mCurrentProcessing.second;
			mCurrentProcessing = std::pair<TNSocket*, SnapshotRequest*>(nullptr, nullptr);
		}
	}

	void RayTracingService::OnMessageReceived(TNSocket* From, TNMessagePtr Message)
	{
		if (Message->GetMessageType() == TNMessageType::MT_Camera)
		{
			TNMessageCameraData* Cam = static_cast<TNMessageCameraData*>(Message.Get());

			const TNMatrix4x4& Mat = Cam->GetTransform();

			Matrix4F Matrix;
			memcpy_s(Matrix.elements, sizeof(Matrix.elements), Mat.m_Components, sizeof(TNMatrix4x4::m_Components));

			//mTRayApp->GetCamera()->SetCameraData(Matrix, Cam->GetFOV(), 1.778f);
		}
		else if (Message->GetMessageType() == TNMessageType::MT_Json) 
		{
			TNMessageJson* jsonMsg = static_cast<TNMessageJson*>(Message.Get());
			std::string jsonString = jsonMsg->GetJsonString().CString();
			auto j3 = nlohmann::json::parse(jsonString);
			bool IsRenderingPaused = j3["isRenderPaused"];
			mTRayApp->SetRenderingState(IsRenderingPaused);
		}
		else
		{
			SnapshotRequest* NewReq = AddNewMessageToRequestQueue(From, Message, false);
		}
	}

	void RayTracingService::OnRequestReceived(TNSocket* From, TNMessagePtr Request)
	{
		SnapshotRequest* NewReq = AddNewMessageToRequestQueue(From, Request, true);

		if (NewReq->IsRequestCompleted())
		{
			RemoveRequestFromList(mRequestsComingIn, From, NewReq);
			AddRequestToList(mRequestsPending, From, NewReq);
		}
	}

	void RayTracingService::OnResponseReceived(TNSocket* From, TNMessagePtr Request)
	{

	}

	void RayTracingService::OnFeedbackReceived(TNSocket* From, TNMessagePtr Request)
	{

	}

	SnapshotRequest* RayTracingService::AddNewMessageToRequestQueue(TNSocket* From, TNMessagePtr Message, bool IsRequest)
	{
		TRequestsFromUsers::iterator itr = mRequestsComingIn.find(From);
		if (itr == mRequestsComingIn.end())
		{
			TUserRequestPair Req = TUserRequestPair(From, TRequestsList());
			mRequestsComingIn.insert(Req);
			itr = mRequestsComingIn.find(From);
		}

		if (itr->second.size() == 0)
		{
			itr->second.push_back(new SnapshotRequest());
		}

		if (IsRequest)
		{
			(*(itr->second.begin()))->OnRequestReceived(Message);
		}
		else
		{
			(*(itr->second.begin()))->OnMessageReceived(Message);
		}

		return (*(itr->second.begin()));
	}

	void RayTracingService::RemoveUserFromList(TRequestsFromUsers& List, TNSocket* Socket) const
	{
		TRequestsFromUsers::iterator itr = List.find(Socket);
		if (itr != List.end())
		{
			TRequestsList& Requests = itr->second;
			for (auto a : Requests)
			{
				delete a;
			}

			List.erase(Socket);
		}
	}

	void RayTracingService::RemoveRequestFromList(TRequestsFromUsers& List, TNSocket* Socket, SnapshotRequest* Request) const
	{
		TRequestsFromUsers::iterator itr = List.find(Socket);
		if (itr != List.end())
		{
			TRequestsList& RequestsList = itr->second;
			RequestsList.remove(Request);
		}
	}

	void RayTracingService::AddRequestToList(TRequestsFromUsers& List, TNSocket* From, SnapshotRequest* Request) const
	{
		TRequestsFromUsers::iterator itr = List.find(From);
		if (itr == List.end())
		{
			TUserRequestPair Req = TUserRequestPair(From, TRequestsList());
			List.insert(Req);
			itr = List.find(From);
		}

		TRequestsList& RequestsList = itr->second;
		RequestsList.push_back(Request);
	}

	std::pair<TNSocket*, SnapshotRequest*> RayTracingService::GetAndRemoveRequestFromList(TRequestsFromUsers& UserRequests) const
	{
		TRequestsFromUsers::iterator itr = UserRequests.begin();
		for (;itr != UserRequests.end(); itr++)
		{
			if (itr->second.size() > 0)
			{
				TRequestsList& List = itr->second;

				SnapshotRequest* Req = *(List.begin());
				TNSocket* From = itr->first;
				List.pop_front();
				return std::pair<TNSocket*, SnapshotRequest*>(From, Req);
			}
		}

		return std::pair<TNSocket*, SnapshotRequest*>(nullptr, nullptr);
	}
}
