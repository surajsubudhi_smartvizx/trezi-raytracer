#pragma once

#ifndef MATERIAL_PARAMETER_H
#define MATERIAL_PARAMETER_H

#include "function_indices.h"

 // Just some hardcoded material parameter system which allows to show a few fundamental BSDFs.
 // Alignment of all data types used here is 4 bytes.
struct MaterialParameter
{
	FunctionIndex		indexBSDF;  // BSDF index to use in the closest hit program												// |  4 bytes
	optix::float3		albedo;     // Albedo, tint, throughput change for specular surfaces. Pick your meaning.				// | 12 bytes
																																// | 
	optix::float3		emission;																								// | 12 bytes
	float				specular;																								// |  4 bytes
	float				roughness;																								// |  4 bytes
	float				metallic;																								// |  4 bytes
																																// | 
	int					albedoID;   // Bindless 2D texture ID used to modulate the albedo color when != RT_TEXTURE_ID_NULL.		// |  4 bytes
	int					metallicTexID;																							// |  4 bytes
	int					roughnessTexID;																							// |  4 bytes
	int					normalTexID;																							// |  4 bytes
	int					bumpTexID;																								// |  4 bytes
	int					specularTexID;																							// |  4 bytes
	int					emissiveTexID;																							// |  4 bytes
																																// | 
	int					cutoutID;   // Bindless 2D texture ID used to calculate the cutout opacity when != RT_TEXTURE_ID_NULL.	// |  4 bytes
	optix::float3		absorption; // Absorption coefficient																	// | 12 bytes
	float				ior;        // Index of refraction																		// |  4 bytes
	unsigned int		flags;      // Thin-walled on/off																		// |  4 bytes
																																// | 
	// Manual padding to 16-byte alignment goes here.																			// | 
	float unused0;																												// |  4 bytes
	//------------------------------------------------------------------------------------------------------------------------------------------
	// Total Bytes																												// | 96 bytes

};

#endif // MATERIAL_PARAMETER_H
